package Exercicio2;

import java.util.Scanner;

public class APP {
    
    //Constante
    static final int MAXCONTA = 20;
   
    //vari�vel comum
    static int index = 0;
    
    //Lista de contas
    static Conta[] lista = new Conta[MAXCONTA];
    
    static Scanner tecla = new Scanner(System.in);

    public static void main(String[] args) {
        int op;
        do {                
            System.out.println("*** MENU PRINCIPAL ***");
            System.out.println("1-Incluir conta");
            System.out.println("2-Sacar");
            System.out.println("3-Depositar");
            System.out.println("4-Listar saldo das Contas");
            System.out.println("5- Excluir Conta");
            System.out.println("6- Passar Cartao");
            System.out.println("7-Consultar Saldo");
            System.out.println("8-Transferencia");
            System.out.println("9-Sair");
            System.out.println("Digite sua op��o: ");
            op = tecla.nextInt(); 
            switch(op){
                case 1: incluirConta(); break;
                case 2: sacarValor(); break;
                case 3: depositarValor(); break;    
                case 4: listarContas(); break;
                case 5: excluirConta(); break;
                case 6: cartao();break;
                case 7: consultarSaldo();break;
                case 8: Transferencia();break;
                case 9: break;
            }
        } while (op!=9);       
    }
    
    
    private static void Transferencia() {
		
    	System.out.println("Digite o n�mero da conta a trasnferir:");
    	int numc1 = tecla.nextInt();
    	System.out.println("Digite o n�mero da conta a a receber:");
    	int numc2 = tecla.nextInt();
    	System.out.println("Digite o valor");
        float valor = tecla.nextFloat();
    	
    	
    	 for (int i = 0; i < lista.length-1; i++) {
             if (numc1 == lista[i].getNumero()){
            	 lista[i].sacar(valor);
            	 double saldoc1 = lista[i].getSaldo();
            	 System.out.println("O saldo da conta trasnferida e: " + saldoc1);
                 break;
             }
         }
    	 for (int i = 0; i < lista.length-1; i++) {
             if (numc2 == lista[i].getNumero()){
            	 lista[i].depositar(valor);
            	 double saldoc2 = lista[i].getSaldo();
            	 System.out.println("O saldo da conta recebida e: " + saldoc2);
            	 break;
             }
         }
    	 System.out.println("Transferencia realizada  com sucesso");
       
       
		
	}


	private static void consultarSaldo() {
		
    	System.out.println("Digite o n�mero da conta:");
        int num = tecla.nextInt();
        for (int i = 0; i < lista.length-1; i++) {
            if (num == lista[i].getNumero()){
                double saldo = lista[i].getSaldo();
                System.out.println("O saldo da conta � " + saldo);
                break;
            }
        }
       
	}


	private static void cartao() {
    	
    	
    	System.out.println("Digite 1 para credito 2 para debito");
        int num = tecla.nextInt();
        if(num == 1) {
        	System.out.println("Digite o numero da conta");
            int numc = tecla.nextInt();
            System.out.println("Digite o valor");
            double valor = tecla.nextDouble();
            for (int i = 0; i < lista.length-1; i++) {
                if (numc == lista[i].getNumero()){
                    lista[i].sacar(valor);
                    break;
                }
               
            }
            System.out.println("Cartao passado com  sucesso");
        }else {
        	System.out.println("Digite o numero da conta");
            int numc = tecla.nextInt();
            System.out.println("Digite o valor");
            double valor = tecla.nextDouble();
            for (int i = 0; i < lista.length-1; i++) {
                if (numc == lista[i].getNumero()){
                    lista[i].sacar(valor);
                    break;
                }
            }
            
            System.out.println("Cartao passado com  sucesso");
        }
        
		
	}


	private static void excluirConta() {
    	System.out.println("Digite o n�mero da conta:");
        int num = tecla.nextInt();
        
        for (int i = 0; i < lista.length-1; i++) {
            if (num == lista[i].getNumero()){
                lista[i] = null;
                System.out.println("Conta excluida com sucesso");
                break;
            }
        }
	}
    
    


	public static void incluirConta(){
        //Entrada
        System.out.println("Digite o n�mero da conta:");
        int num = tecla.nextInt();
        System.out.println("Digite o saldo da conta:");
        double saldo = tecla.nextDouble();
        //Criar o objeto e inserir na lista
        lista[index++] = new Conta(num, saldo);
        System.out.println("Conta cadastrada com sucesso!");
    }
    
    public static void sacarValor(){
        //Entrada
        System.out.println("Digite o n�mero da conta:");
        int num = tecla.nextInt();
        System.out.println("Digite o valor do saque:");
        double valor = tecla.nextDouble();
        
        //Procurar a conta na lista
        for (int i = 0; i < lista.length-1; i++) {
            if (num == lista[i].getNumero()){
                lista[i].sacar(valor);
                break;
            }
        }
    }
    
    public static void depositarValor(){
        //Entrada
        System.out.println("Digite o n�mero da conta:");
        int num = tecla.nextInt();
        System.out.println("Digite o valor do dep�sito:");
        double valor = tecla.nextDouble();
        
        //Procurar a conta na lista
        for (int i = 0; i < lista.length-1; i++) {
            if (num == lista[i].getNumero()){
                lista[i].depositar(valor);
                break;
            }
        }
    }
    
    public static void listarContas(){
        double total = 0;
        System.out.println("N� Conta:........ SALDO:");
        for (int i = 0; i < lista.length-1; i++) {
            if (lista[i] != null){
                System.out.println(lista[i].getNumero()
                                   + "........" +
                                   lista[i].getSaldo());
                //total += lista[i].getSaldo();
                total = total + lista[i].getSaldo();
            }else{
                break;
            }
        }
        System.out.println("Total:........" + total);
    }
    
    
}