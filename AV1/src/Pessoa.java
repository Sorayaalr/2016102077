

public class Pessoa implements Comparable<Pessoa>
{
    public int cod;
    public int idade;
    public String nome;
    
    public Pessoa() {
    }
    
    public Pessoa(final int cod, final int idade, final String nome) {
        this.cod = cod;
        this.idade = idade;
        this.nome = nome;
    }
    
    public int getCod() {
        return this.cod;
    }
    
    public void setCod(final int cod) {
        this.cod = cod;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(final String nome) {
        this.nome = nome;
    }
    
    public int getIdade() {
        return this.idade;
    }
    
    public void setIdade(final int idade) {
        this.idade = idade;
    }
    
    @Override
    public int compareTo(final Pessoa pessoa) {
        if (this.idade < pessoa.getIdade()) {
            return -1;
        }
        if (this.idade > pessoa.getIdade()) {
            return 1;
        }
        return 0;
    }
}