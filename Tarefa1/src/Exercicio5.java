import java.util.Scanner;

public class Exercicio5 {
	
	static Scanner tecla = new Scanner (System.in);
	
	public static void main(String[] args) {
		
		// declara��o de vari�veis
		double comp=0, larg=0, alt=0, area=0, caixas=0;
		
		
		//entrada de dados
		System.out.println("Qual o comprimento da cozinha? ");
		comp = tecla.nextDouble();
		System.out.println("Qual a largura da cozinha? ");
		larg = tecla.nextDouble();
		System.out.println("Qual a altura da cozinha? ");
		alt = tecla.nextDouble();
		
		//processamento dos dados
		area = (comp * alt * 2) + (larg * alt * 2);
		caixas = (area / 1.5);
		
		//saida de dados
		System.out.println("Quantidade de caixas de azulejos para colocar em todas as paredes � : " + (int)caixas);
		
		
	}

}
